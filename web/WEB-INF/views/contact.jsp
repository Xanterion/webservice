<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title>Система принятия решений</title>

</head>
<body>
<h3>Имя системы</h3>
<c:if test="${!empty contactList}">
    <table class="data">
        <tr>
            <th>Имя</th>
            <th>Email</th>
            <th>Телефон</th>
            <th>&nbsp;</th>
        </tr>
        <c:forEach items="${contactList}" var="contact">
            <tr>
                <td>${contact.surname}, ${contact.name}</td>
                <td>${contact.email}</td>
                <td>${contact.tel}</td>
                <td>${contact.userId}</td>
                <td><a href="delete/${contact.userId}">Удалить</a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>