package com.webservice.dao;

import java.util.List;
import java.util.Optional;

public interface DAO<T,ID> {

    Optional<T> getById(ID id);

    List<T> getAll();

    void delete(T entity);

    ID add(T entity);

    void update(T entity);
}
