package com.webservice.dao;

import com.webservice.model.LeadGenScheme;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface LeadGenSchemeDAO extends DAO<LeadGenScheme,Integer>{

    @Override
    List<LeadGenScheme> getAll();

    @Override
    Optional<LeadGenScheme> getById(Integer id);

    @Override
    void delete(LeadGenScheme leadGenScheme);

    @Override
    Integer add(LeadGenScheme leadGenScheme);

    @Override
    void update(LeadGenScheme entity);

    Optional<LeadGenScheme> getByKeysOrderByPriority(Collection<String> keys);

    List<LeadGenScheme> getByKeys(Collection<String> keys);
}
