package com.webservice.dao;

import com.webservice.model.Lead;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface LeadDAO extends DAO<Lead,Long>{

    List<Lead> getByKeys(Collection<String> keys);

    void updateLeadGenId(Long id);
}
