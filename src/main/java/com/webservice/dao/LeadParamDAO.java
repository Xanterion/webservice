package com.webservice.dao;

import com.webservice.model.Lead;
import com.webservice.model.LeadParam;

import java.util.Optional;

public interface LeadParamDAO extends DAO<LeadParam, Long> {

    Optional<LeadParam> getByLeadIdAndKey(Long leadId, String key);

}
