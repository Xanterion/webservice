package com.webservice.dao.impl;

import com.webservice.dao.LeadGenSchemeKeyDAO;
import com.webservice.model.LeadGenSchemeKey;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class LeadGenSchemeKeyDAOImpl extends DAOImpl implements LeadGenSchemeKeyDAO {

    @Override
    public Optional<LeadGenSchemeKey> getById(Long id) {

        List<LeadGenSchemeKey> leadGenSchemes = getSession()
                .createQuery("from LeadGenSchemeKey as leadscheme where leadscheme.id = :id", LeadGenSchemeKey.class)
                .setParameter("id",id)
                .list();
        Optional<LeadGenSchemeKey> result = leadGenSchemes.isEmpty() ? Optional.empty() : Optional.of(leadGenSchemes.get(0));

        return result;
    }

    @Override
    public List<LeadGenSchemeKey> getAll() {
        List<LeadGenSchemeKey> leadGenSchemeKeys = getSession()
                .createQuery("from LeadGenSchemeKey", LeadGenSchemeKey.class)
                .list();

        return leadGenSchemeKeys;
    }

    @Override
    public void delete(LeadGenSchemeKey entity) {
        getSession().delete(entity);
    }

    @Override
    public Long add(LeadGenSchemeKey entity) {
        return (Long) getSession().save(entity);
    }

    @Override
    public void update(LeadGenSchemeKey entity) {
        getSession().update(entity);
    }
}
