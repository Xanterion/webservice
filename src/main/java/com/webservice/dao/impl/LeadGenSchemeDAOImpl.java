package com.webservice.dao.impl;

import com.webservice.dao.LeadGenSchemeDAO;
import com.webservice.model.LeadGenScheme;
import com.webservice.model.LeadGenSchemeKey;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;

@Repository
@Transactional
public class LeadGenSchemeDAOImpl extends DAOImpl implements LeadGenSchemeDAO {

    protected Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public List<LeadGenScheme> getAll() {

        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
        CriteriaQuery<LeadGenScheme> criteriaQuery = criteriaBuilder.createQuery(LeadGenScheme.class);
        criteriaQuery.select(criteriaQuery.from(LeadGenScheme.class));

        List<LeadGenScheme> leadGenSchemes = getSession().createQuery(criteriaQuery).getResultList();
        return leadGenSchemes;
    }

    @Override
    public Optional<LeadGenScheme> getById(Integer id) {

        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
        CriteriaQuery<LeadGenScheme> criteriaQuery = criteriaBuilder.createQuery(LeadGenScheme.class);
        Root<LeadGenScheme> leadGenSchemeRoot = criteriaQuery.from(LeadGenScheme.class);
        Predicate idEquality = criteriaBuilder.equal(leadGenSchemeRoot.get("id"),id);
        criteriaQuery.select(leadGenSchemeRoot).where(idEquality);

        List<LeadGenScheme> leadGenSchemes = getSession().createQuery(criteriaQuery).getResultList();

        Optional<LeadGenScheme> result = leadGenSchemes.isEmpty() ? Optional.empty() : Optional.of(leadGenSchemes.get(0));

        return result;
    }

    @Override
    public List<LeadGenScheme> getByKeys(Collection<String> keys) {

        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<LeadGenScheme> query = cb.createQuery(LeadGenScheme.class);
        Root<LeadGenSchemeKey> schemeRoot = query.from(LeadGenSchemeKey.class);
        Join<LeadGenSchemeKey, LeadGenScheme> schemeKeyJoin = schemeRoot.join("leadGenScheme", JoinType.INNER);
        Expression<String> joinKeyField = schemeRoot.get("key");
        Predicate keyInSet = joinKeyField.in(keys);
        query.select(schemeKeyJoin).where(keyInSet).distinct(true);
        List<LeadGenScheme> foundKeys = getSession().createQuery(query).getResultList();

        return foundKeys;
    }

    @Override
    public Optional<LeadGenScheme> getByKeysOrderByPriority(Collection<String> keys) {

        Optional<LeadGenScheme> leadGenScheme = Optional.empty();
        List<LeadGenScheme> leadGenSchemes = new ArrayList<>();
        List<LeadGenScheme> matches = new ArrayList<>();

        //Извлекаем все схемы и ищем с помощью коллекций
        leadGenSchemes = getSession()
                .createQuery("from  LeadGenScheme", LeadGenScheme.class)
                .list();

        for (LeadGenScheme scheme:leadGenSchemes) {
            List<String> schemeKeys = scheme.getLeadGenSchemeKeys()
                    .stream()
                    .map(LeadGenSchemeKey::getKey)
                    .collect(Collectors.toList());
            if (schemeKeys.size() == keys.size() && schemeKeys.containsAll(keys)) {
                matches.add(scheme);
            }
        }

        matches.sort(Comparator.comparingInt(LeadGenScheme::getPriority).reversed());
        leadGenScheme = matches.isEmpty() ? Optional.empty() : Optional.of(matches.get(0));

        log.info(" java lead search. found matches = " + matches.size() + " priority = " +
                leadGenScheme.map(leadGenScheme1 -> leadGenScheme1.getPriority().toString()).orElse("none"));

        leadGenSchemes = getSession()
                .createQuery("from LeadGenScheme as scheme where size(scheme.leadGenSchemeKeys) = :keysetsize and " +
                        "(select count(key) from LeadGenSchemeKey as keys where keys.leadGenScheme.id = scheme.id and keys.key in (:keyset)) = :keysetsize " +
                        "order by scheme.priority desc ", LeadGenScheme.class)
                .setParameter("keysetsize", keys.size())
                .setParameter("keyset", keys)
                .list();


        leadGenScheme = leadGenSchemes.isEmpty() ? Optional.empty() : Optional.of(leadGenSchemes.get(0));
        log.info(" hql query lead search. found = " + leadGenSchemes.size() + " priority = " +
                leadGenScheme.map(leadGenScheme1 -> leadGenScheme1.getPriority().toString()).orElse("none") );

        /*
        select id_storage from public.param
        where val = any ('{"val1","val2","val3","val4"}')
        group by id_storage
        having count(*) = 4
        * */

        /*Join LeadGenSchemeKey к LeadGenScheme, по по ManyToOne relation leadGenSchemeKeys
        * берем параметр "key" находящийся в таблице LeadGenSchemeKey т.к. дання таблица join - нится к LeadGenScheme
        * */
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<LeadGenSchemeKey> query = cb.createQuery(LeadGenSchemeKey.class);
        Root<LeadGenScheme> schemeRoot = query.from(LeadGenScheme.class);
        Join<LeadGenScheme, LeadGenSchemeKey> schemeKeyJoin = schemeRoot.join("leadGenSchemeKeys", JoinType.INNER);
        Expression<String> joinKeyField = schemeKeyJoin.get("key");
        Predicate keyInSet = joinKeyField.in(keys);
        query.select(schemeKeyJoin).where(keyInSet).orderBy(cb.desc(schemeRoot.get("priority")));
        List<LeadGenSchemeKey> foundKeys = getSession().createQuery(query).getResultList();

        leadGenScheme = foundKeys.isEmpty() ? Optional.empty() : Optional.of(foundKeys.get(0).getLeadGenScheme());

        cb = getSession()
                .getCriteriaBuilder();
        CriteriaQuery<LeadGenScheme> query2 = cb.createQuery(LeadGenScheme.class);
        Root<LeadGenSchemeKey> keyRoot = query2.from(LeadGenSchemeKey.class);
        Join<LeadGenSchemeKey, LeadGenScheme> schemeKeyJoin2 = keyRoot.join("leadGenScheme", JoinType.INNER);
        Expression<String> parentExpression = keyRoot.get("key");
        Predicate parentPredicate = parentExpression.in(keys);
        // Query не имеет distinct т.к. хотя результатов много, требуется единственное значение и наибольшим приоритетом
        query2.select(schemeKeyJoin2).where(parentPredicate).orderBy(cb.desc(schemeKeyJoin2.get("priority")));
        List<LeadGenScheme> foundKeys2 = getSession().createQuery(query2).getResultList();

        leadGenScheme = foundKeys2.isEmpty() ? Optional.empty() : Optional.of(foundKeys2.get(0));

        log.info("criteria api lead search. found = " + Boolean.toString(leadGenScheme.isPresent()));

        return leadGenScheme;
    }

    @Override
    public Integer add(LeadGenScheme leadGenScheme) {
        return (Integer)getSession().save(leadGenScheme);
    }

    @Override
    public void delete(LeadGenScheme leadGenScheme) {
        getSession().delete(leadGenScheme);
    }

    @Override
    public void update(LeadGenScheme entity) {
        getSession().update(entity);
    }
}
