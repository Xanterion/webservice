package com.webservice.dao.impl;

import com.webservice.dao.LeadParamDAO;
import com.webservice.model.LeadParam;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class LeadParamDAOImpl extends DAOImpl implements LeadParamDAO {

    @Override
    public Optional<LeadParam> getById(Long id) {
        List<LeadParam> leadParams = getSession()
                .createQuery("from LeadParam where LeadParam.id = :id", LeadParam.class)
                .setParameter("id",id)
                .list();
        Optional<LeadParam> result = leadParams.isEmpty() ? Optional.empty() : Optional.of(leadParams.get(0));
        return result;
    }

    @Override
    public List<LeadParam> getAll() {
        List<LeadParam> leadParams = getSession()
                .createQuery("from LeadParam", LeadParam.class)
                .list();
        return leadParams;
    }

    @Override
    public Optional<LeadParam> getByLeadIdAndKey(Long leadId, String key) {
        List<LeadParam> leadParams = getSession()
                .createQuery("from LeadParam as lparam where lparam.lead.id = :leadId and lparam.key = :keyvalue", LeadParam.class)
                .setParameter("leadId", leadId)
                .setParameter("keyvalue", key)
                .list();
        Optional<LeadParam> result = leadParams.isEmpty() ? Optional.empty() : Optional.of(leadParams.get(0));
        return result;
    }

    @Override
    public void delete(LeadParam entity) {
        getSession().delete(entity);
    }

    @Override
    public Long add(LeadParam entity) {
        return (Long) getSession().save(entity);
    }

    @Override
    public void update(LeadParam entity) {
        getSession().update(entity);
    }
}
