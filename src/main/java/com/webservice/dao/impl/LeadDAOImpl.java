package com.webservice.dao.impl;

import com.webservice.dao.LeadDAO;
import com.webservice.model.Lead;
import com.webservice.model.LeadGenScheme;
import com.webservice.model.LeadParam;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@Transactional
public class LeadDAOImpl extends DAOImpl implements LeadDAO {

    @Override
    public Optional<Lead> getById(Long id) {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
        CriteriaQuery<Lead> criteriaQuery = criteriaBuilder.createQuery(Lead.class);
        Root<Lead> leadRoot = criteriaQuery.from(Lead.class);
        Predicate idEquality = criteriaBuilder.equal(leadRoot.get("id"),id);
        criteriaQuery.select(leadRoot).where(idEquality);

        List<Lead> leads = getSession().createQuery(criteriaQuery).getResultList();


        Optional<Lead> result = leads.isEmpty() ? Optional.empty() : Optional.of(leads.get(0));
        return result;
    }

    @Override
    public List<Lead> getByKeys(Collection<String> keys) {

        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
        CriteriaQuery<Lead> criteriaQuery = criteriaBuilder.createQuery(Lead.class);
        Root<LeadParam> leadParamRoot = criteriaQuery.from(LeadParam.class);
        Join<LeadParam, Lead> leadParamLeadJoin = leadParamRoot.join("lead");
        Predicate keyInSet = leadParamRoot.get("key").in(keys);
        criteriaQuery.select(leadParamLeadJoin).where(keyInSet).distinct(true);

        List<Lead> leads = getSession().createQuery(criteriaQuery).getResultList();

        return leads;
    }

    @Override
    public List<Lead> getAll() {
        return getSession()
                .createQuery("from Lead ", Lead.class)
                .list();
    }

    @Override
    public void delete(Lead entity) {
        getSession()
                .delete(entity);
    }

    @Override
    public Long add(Lead entity) {
        return (Long) getSession()
                .save(entity);
    }

    @Override
    public void update(Lead entity) {
        getSession()
                .update(entity);
    }

    @Override
    public void updateLeadGenId(Long id) {
        getSession()
                .createQuery("update Lead as lead set lead.leadGenScheme.id = :id", Lead.class)
                .setParameter("id",id)
                .executeUpdate();
    }
}
