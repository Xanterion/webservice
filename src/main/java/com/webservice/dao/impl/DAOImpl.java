package com.webservice.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class DAOImpl {

    @Autowired
    private SessionFactory sessionFactory;

    protected CriteriaBuilder getCriteriaBuilder(){
        return sessionFactory.getCurrentSession().getCriteriaBuilder();
    }

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }
}
