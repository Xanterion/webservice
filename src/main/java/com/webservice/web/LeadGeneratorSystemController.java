package com.webservice.web;


import com.webservice.dao.LeadGenSchemeDAO;
import com.webservice.model.LeadGenScheme;
import com.webservice.model.Lead;
import com.webservice.model.LeadGenSchemeKey;
import com.webservice.model.LeadParam;
import com.webservice.service.LeadGenSchemeService;
import com.webservice.service.LeadParamService;
import com.webservice.service.LeadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("leadgen")
public class LeadGeneratorSystemController {

    protected Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private LeadGenSchemeService leadGenSchemeService;

    @Autowired
    private LeadService leadService;

    @Autowired
    private LeadGenSchemeDAO leadGenSchemeDAO;

    @Autowired
    private LeadParamService leadParamService;

    @GetMapping(path = "priority/{priorityNumber}")
    public ResponseEntity<?> setLeadPriority(@PathVariable("priorityNumber") Integer number, @RequestParam Map<String,String> params){

        /*if (params.isEmpty()){
            return new ResponseEntity<>("LeadGen not found", HttpStatus.BAD_REQUEST);
        } else{
            String leadId = LeadGenScheme.generateId(params.keySet());
            Optional<LeadGenScheme> existingModel = leadGenSchemeService.getById(leadId);
            if (existingModel.isPresent()) {
                LeadGenScheme leadGenScheme = existingModel.get();
                leadGenScheme.setPriority(number);
                leadGenSchemeService.update(leadGenScheme);
                return new ResponseEntity<>("Priority set - " + number, HttpStatus.OK);
            }
            return new ResponseEntity<>("LeadGen not found ", HttpStatus.BAD_REQUEST);
        }*/
        return new ResponseEntity<>("LeadGen not found ", HttpStatus.BAD_REQUEST);
    }

    @GetMapping(path = "/new")
    public ResponseEntity<?> leadGenAddRequest(@RequestParam Map<String,String> leadKeysFromRequest, HttpServletRequest request){

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie:cookies) {
                if (!cookie.getName().equals("JSESSIONID"))
                    leadKeysFromRequest.put(cookie.getName(), cookie.getValue());
            }
        }

        if (leadKeysFromRequest.isEmpty()) {
            log.debug("LeadGenAdd: Lead request params is empty");
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        } else {
            log.debug("LeadGenAdd: Lead request params is not empty");
            LeadGenScheme requestLeadGenScheme;
            Set<String> keySet = leadKeysFromRequest.keySet();
            Optional<LeadGenScheme> existingModel = leadGenSchemeService.getByKeysOrderByPriority(keySet);
            if (existingModel.isPresent() && false) {
                log.debug("LeadGenAdd: Leadgenerator already exist");
                requestLeadGenScheme = existingModel.get();
                return new ResponseEntity<>("LeadGenAdd: Leadgenerator already exist " + requestLeadGenScheme.toString(), HttpStatus.BAD_REQUEST);
            } else {
                log.debug("LeadGenAdd: Adding new leadgenerator and lead");
                requestLeadGenScheme = leadGenSchemeService.saveScheme(leadKeysFromRequest.keySet());

                List<Lead> leads = leadService.getByKeys(keySet);
                for (Lead lead:leads) {
                    lead.setLeadGenScheme(requestLeadGenScheme);
                    leadService.update(lead);
                }

                return new ResponseEntity<>("LeadGenAdd: added params for lead " + requestLeadGenScheme.toString(), HttpStatus.OK);
            }
        }
    }

    @GetMapping(path = "/addlead")
    public ModelAndView leadAdd(@RequestParam Map<String, String> leadKeysFromRequest, HttpServletRequest request){

        final String nameAttr = "name";
        final String messageAttr = "message";
        ModelAndView modelAndView = new ModelAndView("systemname");
        try {
            //Map<String, String> leadKeysFromRequest = request.getParameterMap();
            // Сбор параметров из cookie
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie:cookies) {
                    if (!cookie.getName().equals("JSESSIONID"))
                        leadKeysFromRequest.put(cookie.getName(), cookie.getValue());
                }
            }

            if (leadKeysFromRequest.isEmpty()) {
                log.debug("LeadAdd: Lead request params is empty");
                modelAndView.addObject(nameAttr,"none");
                modelAndView.addObject(messageAttr, "Lead hasn't parameters");
                //return new ResponseEntity<>(modelAndView, HttpStatus.OK);
                //return modelAndView;
            } else {
                log.debug("LeadAdd: Lead request params is not empty");
                Set<String> keySet = leadKeysFromRequest.keySet();

                Optional<LeadGenScheme> foundLeadGen = leadGenSchemeService.getByKeysOrderByPriority(keySet);
                boolean leadGenIsOff = foundLeadGen.isPresent() && !foundLeadGen.get().getWorking();
                Lead lead;
                if ( !foundLeadGen.isPresent() || leadGenIsOff) {
                    lead = leadService.addLead(foundLeadGen.orElse(null), leadKeysFromRequest);
                    modelAndView.addObject(nameAttr,lead.toString());
                    modelAndView.addObject(messageAttr, "Lead Scheme doesn't work or not exist");
                    //return new ResponseEntity<>(modelAndView, HttpStatus.OK);
                } else {
                    lead = leadService.addLead(foundLeadGen.get(), leadKeysFromRequest);
                    modelAndView.addObject(nameAttr, lead.toString());
                    modelAndView.addObject(messageAttr, "Lead added to existing scheme with priority = " + foundLeadGen.get().getPriority());
                    //return new ResponseEntity<>(modelAndView, HttpStatus.OK);
                }
            }
            return modelAndView;
        } catch (Exception e) {
            e.printStackTrace();
            modelAndView.addObject(nameAttr, "none");
            modelAndView.addObject(messageAttr, "error" + e.getMessage());
            //return new ResponseEntity<>(modelAndView, HttpStatus.BAD_REQUEST);
            return modelAndView;
        }

    }

    @GetMapping(path = "test")
    public String test(){

        Boolean testResult = true;

        return testResult.toString();
    }

}
