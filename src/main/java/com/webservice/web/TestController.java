package com.webservice.web;

import com.webservice.model.LeadGenScheme;
import com.webservice.service.LeadGenSchemeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping(path = "test")
public class TestController {

    protected Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private LeadGenSchemeService leadGenSchemeService;

    @GetMapping(path = "leadin")
    public String insertTest(){
        log.info("emptytest");
        return "complete";
    }

}
