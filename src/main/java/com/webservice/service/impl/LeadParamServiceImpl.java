package com.webservice.service.impl;

import com.webservice.dao.LeadParamDAO;
import com.webservice.model.LeadGenScheme;
import com.webservice.model.LeadParam;
import com.webservice.service.LeadParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LeadParamServiceImpl implements LeadParamService {

    @Autowired
    private LeadParamDAO leadParamDAO;

    @Override
    public void add(LeadParam entity) {
        leadParamDAO.add(entity);
    }

    @Override
    public List<LeadParam> getAll() {
        return leadParamDAO.getAll();
    }

    @Override
    public Optional<LeadParam> getById(Long id) {
        return leadParamDAO.getById(id);
    }

    @Override
    public void update(LeadParam entity) {

    }

    @Override
    public void delete(LeadParam entity) {

    }
}
