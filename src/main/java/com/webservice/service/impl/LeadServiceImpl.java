package com.webservice.service.impl;

import com.webservice.dao.LeadDAO;
import com.webservice.dao.LeadParamDAO;
import com.webservice.model.Lead;
import com.webservice.model.LeadGenScheme;
import com.webservice.model.LeadParam;
import com.webservice.service.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LeadServiceImpl implements LeadService {

    @Autowired
    private LeadDAO leadDAO;

    @Autowired
    private LeadParamDAO leadParamDAO;

    @Override
    public void add(Lead entity) {
        leadDAO.add(entity);
    }

    @Override
    public Lead addLead(LeadGenScheme leadGenScheme, Map<String, String> params) {
        Lead lead = new Lead();
        lead.setLeadGenScheme(leadGenScheme);
        add(lead);
        List<LeadParam> leadParams = new ArrayList<>();
        for (Map.Entry<String, String> pair: params.entrySet()) {
            LeadParam leadParam = new LeadParam();
            leadParam.setLead(lead);
            leadParam.setKey(pair.getKey());
            leadParam.setValue(pair.getValue());
            leadParamDAO.add(leadParam);
            leadParams.add(leadParam);
        }
        lead.setLeadParams(leadParams);
        return lead;
    }

    @Override
    public List<Lead> getByKeys(Collection<String> keys) {
        return leadDAO.getByKeys(keys);
    }

    @Override
    public List<Lead> getAll() {
        return leadDAO.getAll();
    }

    @Override
    public Optional<Lead> getById(Long id) {
        return leadDAO.getById(id);
    }

    @Override
    public void update(Lead entity) {
        leadDAO.update(entity);
    }

    @Override
    public void delete(Lead entity) {
        leadDAO.delete(entity);
    }

    @Override
    public void updateLeadGenId(Long id) {
        leadDAO.updateLeadGenId(id);
    }
}
