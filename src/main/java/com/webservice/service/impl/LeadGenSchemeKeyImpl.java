package com.webservice.service.impl;

import com.webservice.dao.LeadGenSchemeKeyDAO;
import com.webservice.model.LeadGenScheme;
import com.webservice.model.LeadGenSchemeKey;
import com.webservice.service.LeadGenSchemeKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LeadGenSchemeKeyImpl implements LeadGenSchemeKeyService {

    @Autowired
    private LeadGenSchemeKeyDAO leadGenSchemeKeyDAO;

    @Override
    public void add(LeadGenSchemeKey entity) {
        leadGenSchemeKeyDAO.add(entity);
    }



    @Override
    public List<LeadGenSchemeKey> getAll() {
        return leadGenSchemeKeyDAO.getAll();
    }

    @Override
    public void delete(LeadGenSchemeKey entity) {
        leadGenSchemeKeyDAO.delete(entity);
    }

    @Override
    public Optional<LeadGenSchemeKey> getById(Long id) {
        return leadGenSchemeKeyDAO.getById(id);
    }

    @Override
    public void update(LeadGenSchemeKey entity) {
        leadGenSchemeKeyDAO.update(entity);
    }
}
