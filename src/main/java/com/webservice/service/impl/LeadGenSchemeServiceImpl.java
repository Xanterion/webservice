package com.webservice.service.impl;

import com.webservice.dao.LeadGenSchemeDAO;
import com.webservice.dao.LeadGenSchemeKeyDAO;
import com.webservice.model.Lead;
import com.webservice.model.LeadGenScheme;
import com.webservice.model.LeadGenSchemeKey;
import com.webservice.model.LeadParam;
import com.webservice.service.LeadGenSchemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LeadGenSchemeServiceImpl implements LeadGenSchemeService {

    @Autowired
    private LeadGenSchemeDAO leadGenSchemeDAO;

    @Autowired
    private LeadGenSchemeKeyDAO leadGenSchemeKeyDAO;

    @Override
    public void add(LeadGenScheme leadGenScheme) {
        leadGenSchemeDAO.add(leadGenScheme);
    }

    @Override
    public LeadGenScheme saveScheme(Collection<String> keys) {
        LeadGenScheme leadGenScheme = new LeadGenScheme();
        add(leadGenScheme);
        LeadGenSchemeKey leadGenSchemeKey;
        for (String key:keys) {
            leadGenSchemeKey = new LeadGenSchemeKey();
            leadGenSchemeKey.setKey(key);
            leadGenSchemeKey.setLeadGenScheme(leadGenScheme);
            leadGenSchemeKeyDAO.add(leadGenSchemeKey);
        }
        return leadGenScheme;
    }

    @Override
    public List<LeadGenScheme> getAll() {
        return leadGenSchemeDAO.getAll();
    }

    @Override
    public void delete(LeadGenScheme leadGenScheme) {
        leadGenSchemeDAO.delete(leadGenScheme);
    }

    @Override
    public Optional<LeadGenScheme> getById(Integer id) {
        return leadGenSchemeDAO.getById(id);
    }

    @Override
    public void update(LeadGenScheme leadGenScheme) {
        leadGenSchemeDAO.update(leadGenScheme);
    }

    @Override
    public Optional<LeadGenScheme> getByKeysOrderByPriority(Collection<String> keys) {
        return leadGenSchemeDAO.getByKeysOrderByPriority(keys);
    }

    @Override
    public List<LeadGenScheme> getByKeys(Collection<String> keys) {
        return leadGenSchemeDAO.getByKeys(keys);
    }

    @Override
    public List<LeadGenScheme> getByKeys(Lead lead) {
        return getByKeys(lead.getLeadParams()
                .stream()
                .map(LeadParam::getKey)
                .collect(Collectors.toList()));
    }

    @Override
    public List<LeadGenScheme> getByKeys(LeadGenScheme leadGenScheme) {
        return getByKeys(leadGenScheme.getLeadGenSchemeKeys()
                .stream()
                .map(LeadGenSchemeKey::getKey)
                .collect(Collectors.toList()));
    }
}
