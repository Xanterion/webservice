package com.webservice.service;

import com.webservice.model.Lead;
import com.webservice.model.LeadGenScheme;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface LeadService extends Service<Lead, Long> {

    List<Lead> getByKeys(Collection<String> keys);

    void updateLeadGenId(Long id);

    Lead addLead(LeadGenScheme leadGenScheme, Map<String, String> params);

}
