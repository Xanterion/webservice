package com.webservice.service;

import com.webservice.model.LeadGenScheme;

import java.util.List;
import java.util.Optional;

public interface Service<T,ID> {

    void add(T entity);

    List<T> getAll();

    Optional<T> getById(ID id);

    void update(T entity);

    void delete(T entity);
}
