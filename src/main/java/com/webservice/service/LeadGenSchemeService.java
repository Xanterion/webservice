package com.webservice.service;

import com.webservice.model.Lead;
import com.webservice.model.LeadGenScheme;
import com.webservice.model.LeadGenSchemeKey;
import com.webservice.model.LeadParam;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface LeadGenSchemeService extends Service<LeadGenScheme, Integer>{


    Optional<LeadGenScheme> getByKeysOrderByPriority(Collection<String> keys);

    List<LeadGenScheme> getByKeys(Collection<String> keys);

    List<LeadGenScheme> getByKeys(Lead lead);

    List<LeadGenScheme> getByKeys(LeadGenScheme leadGenScheme);

    LeadGenScheme saveScheme(Collection<String> keys);
}
