package com.webservice.model;


import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "lead_gen_scheme", schema = "public")
public class LeadGenScheme {

    private static final Integer maxPriority = 100;

    private static final Integer minPriority = 1;

    //Оставить простой генерируемый ид
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * Параметры лида могут быть как в переменных запроса так и в cookies.
     * +/- учитывать что возможно параметры могут быть одинаковыми, в определении нужного участвует приоритет
     * */


    @Column( nullable = false)
    private Boolean working = true;

    //Приоритет
    @Column(nullable = false)
    private Integer priority = minPriority;

    @OneToMany(mappedBy = "leadGenScheme", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Lead> leads;

    @OneToMany(mappedBy = "leadGenScheme", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<LeadGenSchemeKey> leadGenSchemeKeys;

    public LeadGenScheme() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getWorking() {
        return working;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<Lead> getLeads() {
        return leads;
    }

    public void setLeads(List<Lead> leads) {
        this.leads = leads;
    }

    public List<LeadGenSchemeKey> getLeadGenSchemeKeys() {
        return leadGenSchemeKeys;
    }

    public void setLeadGenSchemeKeys(List<LeadGenSchemeKey> leadGenSchemeKeys) {
        this.leadGenSchemeKeys = leadGenSchemeKeys;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeadGenScheme that = (LeadGenScheme) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(working, that.working) &&
                Objects.equals(priority, that.priority);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, working, priority);
    }
}
