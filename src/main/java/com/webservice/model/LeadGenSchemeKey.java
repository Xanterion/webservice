package com.webservice.model;


import javax.persistence.*;

@Entity
@Table(name = "lead_gen_scheme_key")
public class LeadGenSchemeKey {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String key;

    @ManyToOne
    @JoinColumn(name = "lead_gen_scheme_id", nullable = false)
    private LeadGenScheme leadGenScheme;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public LeadGenScheme getLeadGenScheme() {
        return leadGenScheme;
    }

    public void setLeadGenScheme(LeadGenScheme leadGenScheme) {
        this.leadGenScheme = leadGenScheme;
    }

    @Override
    public boolean equals(Object obj) {

        if (this==obj) {
            return true;
        } else if (!(obj instanceof LeadGenSchemeKey)) {
            return false;
        }

        final LeadGenSchemeKey key = (LeadGenSchemeKey)obj;
        return key.getKey().equals(this.key);
    }
}
