package com.webservice.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Entity
@Table(name = "lead", schema = "public")
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "lead_generator_id")
    private LeadGenScheme leadGenScheme;

    @OneToMany(mappedBy = "lead", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<LeadParam> leadParams;

    public Lead() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long leadParamsConcat) {
        this.id = leadParamsConcat;
    }

    public LeadGenScheme getLeadGenScheme() {
        return leadGenScheme;
    }

    public void setLeadGenScheme(LeadGenScheme leadGenScheme) {
        this.leadGenScheme = leadGenScheme;
    }

    public List<LeadParam> getLeadParams() {
        return leadParams;
    }

    public void setLeadParams(List<LeadParam> leadParams) {
        this.leadParams = leadParams;
    }

    public Map<String, String> getLeadParamsAsMap(){
        Map<String, String> map = new HashMap<>();
        for (LeadParam leadParam : leadParams) {
            map.put(leadParam.getKey(), leadParam.getValue());
        }
        return map;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        for (LeadParam leadParam:leadParams) {
            stringBuilder.append(leadParam.getKey()).append(" ");
        }
        return stringBuilder.toString();
    }
}
