package com.webservice.configuration.test;

import com.webservice.dao.LeadGenSchemeDAO;
import com.webservice.dao.impl.LeadGenSchemeDAOImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LeadGenSchemeDAOTestConfig {

    @Bean
    public LeadGenSchemeDAO leadGenSchemeDAO(){
        return new LeadGenSchemeDAOImpl();
    }

}
