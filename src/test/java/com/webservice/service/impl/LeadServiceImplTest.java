package com.webservice.service.impl;

import com.webservice.configuration.test.TestDataBaseConfig;
import com.webservice.model.Lead;
import com.webservice.model.LeadParam;
import com.webservice.service.LeadService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.Assert.*;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestDataBaseConfig.class})
public class LeadServiceImplTest {


    @Autowired
    private LeadService leadService;

    @Test
    @Transactional
    @Rollback(true)
    public void addLeadWithoutScheme() {

        Map<String, String> params = new HashMap<>();
        params.put("abs","1");
        params.put("yet","2");
        Lead lead = leadService.addLead(null,params);
        List<LeadParam> leadParams = lead.getLeadParams();
        leadParams.sort(Comparator.comparingLong((value -> Long.valueOf(value.getValue()))));
        Assert.assertEquals(leadParams.get(0).getKey(),"abs");

    }
}