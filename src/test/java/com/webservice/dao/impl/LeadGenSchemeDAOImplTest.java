package com.webservice.dao.impl;

import com.webservice.configuration.test.LeadGenSchemeDAOTestConfig;
import com.webservice.configuration.test.TestDataBaseConfig;
import com.webservice.dao.LeadGenSchemeDAO;
import com.webservice.model.LeadGenScheme;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;


@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestDataBaseConfig.class})
public class LeadGenSchemeDAOImplTest {

    @Autowired
    private LeadGenSchemeDAO leadGenSchemeDAO;

    @Test
    @Transactional
    @Rollback(true)
    public void getAll() {

        LeadGenScheme leadGenScheme1 = new LeadGenScheme();
        leadGenScheme1.setPriority(5);
        leadGenScheme1.setWorking(false);

        LeadGenScheme leadGenScheme2 = new LeadGenScheme();
        leadGenScheme2.setPriority(3);
        leadGenSchemeDAO.add(leadGenScheme1);
        leadGenSchemeDAO.add(leadGenScheme2);
        List<LeadGenScheme> leadGenSchemes = leadGenSchemeDAO.getAll();
        leadGenSchemes.sort(Comparator.comparingInt(LeadGenScheme::getPriority).reversed());
        Assert.assertEquals((long)5, (long)leadGenSchemes.get(0).getPriority());
    }

    @Test
    public void getById() {
    }
}