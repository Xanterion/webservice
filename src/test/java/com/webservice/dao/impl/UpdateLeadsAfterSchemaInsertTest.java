package com.webservice.dao.impl;

import com.webservice.configuration.test.TestDataBaseConfig;
import com.webservice.model.Lead;
import com.webservice.model.LeadGenScheme;
import com.webservice.model.LeadGenSchemeKey;
import com.webservice.model.LeadParam;
import com.webservice.service.LeadGenSchemeService;
import com.webservice.service.LeadParamService;
import com.webservice.service.LeadService;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestDataBaseConfig.class})
public class UpdateLeadsAfterSchemaInsertTest {

    @Autowired
    private LeadService leadService;

    @Autowired
    private LeadGenSchemeService leadGenSchemeService;

    @Autowired
    private LeadParamService leadParamService;

    @Autowired
    private SessionFactory sessionFactory;

    private Map<String, String> params = new HashMap<>();

    @Before
    public void setUp(){
        List<Lead> leads = Arrays.asList(new Lead(), new Lead());

        params.put("one","1");
        params.put("two","2");

        for (Lead lead: leads) {
            leadService.add(lead);
            for (Map.Entry<String, String> pairs:params.entrySet()) {
                LeadParam leadParam = new LeadParam();
                leadParam.setKey(pairs.getKey());
                leadParam.setValue(pairs.getValue());
                leadParam.setLead(lead);
                leadParamService.add(leadParam);
            }
        }
    }
    //todo необходим мгновенный update для relation полей классов
    @Test
    @Transactional
    @Rollback(true)
    public void test() {

        List<Lead> leads = leadService.getAll();

        LeadGenScheme leadGenScheme = new LeadGenScheme();
        leadGenSchemeService.add(leadGenScheme);
        for (LeadParam leadParam:leads.get(0).getLeadParams()) {
            LeadGenSchemeKey leadGenSchemeKey = new LeadGenSchemeKey();
            leadGenSchemeKey.setKey(leadParam.getKey());
            leadGenSchemeKey.setLeadGenScheme(leadGenScheme);
        }

        Set<String> keys =params.keySet();

        leadGenScheme = leadGenSchemeService.saveScheme(keys);
        leads = leadService.getByKeys(keys);
        Assert.assertNotNull(leads.get(0).getLeadGenScheme());
    }

}
